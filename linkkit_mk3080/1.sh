#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : 1.sh
# * Author     : 上上签
# * Date       : 2023-12-05
# * ======================================================
##

BIN_FILE1=out/linkkit_mk3080@mk3080/binary/linkkit_mk3080@mk3080_ota.bin
BIN_FILE2=out/linkkit_mk3080@mk3080/binary/linkkit_mk3080@mk3080.bin

TARGET_DIR=~/1sharedfiles

aos make
cp -pvf ${BIN_FILE1} ${TARGET_DIR}
cp -pvf ${BIN_FILE2} ${TARGET_DIR}
