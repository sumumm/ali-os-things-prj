/*
 * Copyright (C) 2015-2018 Alibaba Group Holding Limited
 */
#ifndef __APP_ENTRY_H__
#define __APP_ENTRY_H__

#include "aos/kernel.h"

typedef struct {
    int argc;
    char **argv;
}app_main_paras_t;

#define MK3080_PWM2 PORT_PWM_3
typedef enum __LED_PORT_{
    RED_LED = 0, // PA14
    GRE_LED = 1, // PA15
    BLU_LED = 6, // PA00
}LED_PORT;

int linkkit_main(void *paras);

extern int func_led_ctrl(int ledNum, int status);
extern void func_pwm_start(void);
extern void func_pwm_duty_set(uint32_t dutyCycle);
#endif