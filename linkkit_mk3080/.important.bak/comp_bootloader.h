//================This is split line================
// KEYWORD: COMPONENT NAME IS  bootloader

// #if AOS_CREATE_PROJECT

// description:CAN NOT BE MODIFIED
// #define AOS_COMP_BOOTLOADER 1 // type: bool

// #endif

// #if !AOS_CREATE_PROJECT

// description:BootLoader Support
// #define AOS_COMP_UDATA 0 // type: bool

// #endif

// description:Boot Updater Support
// #define AOS_COMP_UPDATER 1 // type: bool

// #if AOS_COMP_UPDATER

// description:OTA BootLoader Watch Dog Timeout(s)
// #define OTA_2NDBOOT_WDG_TIMEOUT 120 // type: int

// description:OTA BootLoader XZ upgrade buffer size(byte)
// #define OTA_2NDBOOT_XZ_BUF_SIZE 2048 // type: int

// #endif

// description:Ymodem Support
// #define AOS_COMP_YMODEM 1 // type: bool

