//================This is split line================
// KEYWORD: COMPONENT NAME IS  linkkit_mk3080

// description:Linkkit Demo
// #define AOS_APP_LINKKIT_MK3080 0 // type: bool

// #if AOS_APP_LINKKIT_MK3080

// description:Enable OTA
// #define ENABLE_AOS_OTA 0 // type: bool

// description:SOLO
// #define LINKKIT_DEMO_CONFIG_CASE_SOLO 0 // type: bool

// description:Sched
// #define LINKKIT_DEMO_CONFIG_CASE_SCHED 0 // type: bool

// description:Cntdown
// #define LINKKIT_DEMO_CONFIG_CASE_CNTDOWN 0 // type: bool

// description:Show Free Heap Duration
// #define LINKKIT_DEMO_CONFIG_PRINT_HEAP 0 // type: bool

// description:BLE Enable, depends on AWSS_SUPPORT_BLE_PROVISION
// #define BLE 1 // type: bool

// description:Firmware Version
// #define SYSINFO_APP_VERSION "app-1.0.0-20200214.140831" // type: string

// #endif

// description:CAN NOT BE MODIFIED
// #define AOS_CREATE_PROJECT 1 // type: bool

// description:CAN NOT BE MODIFIED
// #define AOS_BUILD_BOARD "mk3080" // type: string

// description:CAN NOT BE MODIFIED
// #define AOS_BUILD_APP "linkkit_mk3080" // type: string

// description:CAN NOT BE MODIFIED
// #define AOS_SDK_PATH "" // type: string

// description:CAN NOT BE MODIFIED
// #define USER_APP_PATH "" // type: string

// #if ((!AOS_BOARD_ESP8266))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((AWSS_SUPPORT_BLE_PROVISION))

// #endif

// #if (((AWSS_SUPPORT_BLE_PROVISION) && CONFIG_COMP_BZ_BUILTIN_HAL))

// #endif

// #if ((((AWSS_SUPPORT_BLE_PROVISION) && CONFIG_COMP_BZ_BUILTIN_HAL) && CONFIG_COMP_BZ_HAL_BLESTACK))

// #endif

// #if ((((AWSS_SUPPORT_BLE_PROVISION) && CONFIG_COMP_BZ_BUILTIN_HAL) && CONFIG_COMP_BZ_HAL_BLESTACK))

// #endif

// #if ((ULOG_CONFIG_FS_FATFS_FLASH || ULOG_CONFIG_FS_FATFS_SD))

// #endif

// #if (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((SUPPORT_TLS || COAP_DTLS_SUPPORT))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((USE_ITLS) || (ENABLE_AOS_OTA) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_ITLS))

// #endif

// #if ((USE_MBEDTLS || COAP_DTLS_SUPPORT) || (KV_CONFIG_SECURE_CRYPT_IMPL) || (ENABLE_AOS_OTA) || (((AWSS_SUPPORT_BLE_PROVISION) && CONFIG_COMP_BZ_BUILTIN_HAL) && CONFIG_COMP_BZ_HAL_CRYPTO_MEBDTLS) || (((ENABLE_AOS_OTA) && !OTA_CONFIG_BLE) && CONFIG_HTTP_SECURE_TLS))

// #endif

// #if ((ENABLE_AOS_OTA))

// #endif

// #if ((ULOG_CONFIG_FS_SPIFFS))

// #endif

// #if ((ULOG_CONFIG_UPLOAD || ULOG_CONFIG_POP_CLOUD) || (UND_CONFIG_USE_UAGENT))

// #endif

