//================This is split line================
// KEYWORD: COMPONENT NAME IS  board_mk3080

// description:MK3080
// #define AOS_BOARD_MK3080 0 // type: bool

// #if AOS_BOARD_MK3080

// description:Locate SPIFFS for using
// #define MK3080_CONFIG_SPIFFS 0 // type: bool

// #if MK3080_CONFIG_SPIFFS

// description:64KB
// #define MK3080_CONFIG_SPIFFS_SIZE_64KB 0 // type: bool

// description:128KB
// #define MK3080_CONFIG_SPIFFS_SIZE_128KB 0 // type: bool

// description:256KB
// #define MK3080_CONFIG_SPIFFS_SIZE_256KB 0 // type: bool

// description:CAN NOT BE MODIFIED
// #define CONFIG_SPIFFS_PHYS_SZ 262144 // type: int

// #endif

// description:CAN NOT BE MODIFIED
// #define BSP_SUPPORT_UART 1 // type: bool

// description:CAN NOT BE MODIFIED
// #define BSP_SUPPORT_GPIO 1 // type: bool

// description:CAN NOT BE MODIFIED
// #define BSP_SUPPORT_TIMER 1 // type: bool

// description:CAN NOT BE MODIFIED
// #define BSP_SUPPORT_FLASH 1 // type: bool

// description:CAN NOT BE MODIFIED
// #define BSP_SUPPORT_WDG 1 // type: bool

// description:CAN NOT BE MODIFIED
// #define BSP_SUPPORT_WIFI 1 // type: bool

// description:CAN NOT BE MODIFIED
// #define CONFIG_LS_DEBUG 0 // type: bool

// description:CAN NOT BE MODIFIED
// #define CONFIG_LS_ID2_OTP 1 // type: bool

// description:CAN NOT BE MODIFIED
// #define CONFIG_LS_KM_SE 0 // type: bool

// description:CAN NOT BE MODIFIED
// #define CONFIG_LS_KM_TEE 0 // type: bool

// description:CAN NOT BE MODIFIED
// #define BSP_SUPPORT_WIFI_LOWPOWER 1 // type: bool

// #endif

