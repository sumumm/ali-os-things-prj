NAME := helloworld_mk3080

$(NAME)_MBINS_TYPE := app
$(NAME)_VERSION := 1.0.1
$(NAME)_SUMMARY := helloworld demo
# $(NAME)_SOURCES := maintask.c appdemo.c
$(NAME)_SOURCES := maintask.c ./example/09_pwm2.c


$(NAME)_COMPONENTS += osal_aos

GLOBAL_DEFINES += AOS_NO_WIFI

$(NAME)_INCLUDES += ./

# components added by include c header file in the source code. DO NOT EDIT!
$(NAME)_COMPONENTS_CUSTOMIZED := cli

# add components name manually here if you want to import some components
$(NAME)_COMPONENTS_CUSTOMIZED +=

# tell build system to add components above. DO NOT EDIT!
$(NAME)_COMPONENTS += $($(NAME)_COMPONENTS_CUSTOMIZED)

# SYS_INCDIRS 	:= ./sys/sys_led \
#                    ./sys/sys_key \
#                    ./sys/sys_task
				   			   
# SYS_SRCDIRS		:= ./sys/sys_led/sys_led.c \
#                    ./sys/sys_key/sys_key.c \
#                    ./sys/sys_task/sys_task.c

# $(NAME)_SOURCES  += $(SYS_SRCDIRS)
# $(NAME)_INCLUDES += $(SYS_INCDIRS)