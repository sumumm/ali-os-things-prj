## Contents

```sh
helloworld_mk3080
├── Config.in       # kconfig file
├── aos.mk          # aos build system file(for make)
├── app.config      # aos app config file
├── main.c          # program entry main()
└── app_main.c      # application entry application_start()
```

## Introduction

The **helloworld_mk3080** ...

### Dependencies

### Supported Boards

- mk3080

### Build

```sh
# build
aos make
```
