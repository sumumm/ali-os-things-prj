/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : sys_key.h
 * Author     : 上上签
 * Date       : 2023-12-01
 * Version    : 
 * Description: 
 * ======================================================
 */

#ifndef __SYS_KEY_H__
#define __SYS_KEY_H__

#include <aos/kernel.h>

#define KEY1_PORT 8 // PA5
#define KEY2_PORT 2 // PA22
#define KEY3_PORT 4 // PA23

extern int sys_key_init(void);
extern uint32_t sys_key_value_get(void);

extern int sys_key_int_init(void);
#endif /* __SYS_KEY_H__ */


