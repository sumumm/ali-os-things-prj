/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : sys_key.c
 * Author     : 上上签
 * Date       : 2023-12-01
 * Version    : 
 * Description: 
 * ======================================================
 */

#include "aos/hal/gpio.h"

#include "sys_key.h"
#include "sys_led.h"

static gpio_dev_t  key_gpio_dev[3] = {0};
static gpio_irq_trigger_t  key_trigger[3] = {0};
int sys_key_init(void)
{
    key_gpio_dev[0].port = KEY1_PORT;
    // 根据原理图分析配置位输入上拉模式
    key_gpio_dev[0].config = INPUT_PULL_UP;
    key_gpio_dev[0].priv = NULL;
    // key 初始化
    hal_gpio_init(&key_gpio_dev);

    return 0;
}

uint32_t sys_key_value_get(void)
{
    uint32_t key_value;
    hal_gpio_input_get(&key_gpio_dev, &key_value);
    return key_value;
}

void key_int_irq_handler(void *arg)
{
    uint32_t key_value;
    hal_gpio_input_get(&key_gpio_dev, key_value);
    if(!key_value)
    {
        printf("Interrupt generation!\r\n");
        sys_led_output_toggle(RED_LED);
    }
}

int sys_key_int_init(void)
{
    key_gpio_dev[0].port = KEY1_PORT;
    // 根据原理图分析配置位输入上拉模式
    key_gpio_dev[0].config = INPUT_PULL_UP;
    key_gpio_dev[0].priv = NULL;
    // key 初始化
    hal_gpio_init(&key_gpio_dev);

    //使能按键中断
    key_trigger[0] = IRQ_TRIGGER_FALLING_EDGE;
    hal_gpio_enable_irq(&key_gpio_dev[0], key_trigger[0], key_int_irq_handler, NULL);

    return 0;
}