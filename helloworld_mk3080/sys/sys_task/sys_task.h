/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : sys_task.h
 * Author     : 上上签
 * Date       : 2023-12-02
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __SYS_TASK_H__
#define __SYS_TASK_H__

#define TASK_NUM       2
#define MUDULE_NAME    "task_management"

extern int sys_task_init(void);
#endif /* __SYS_TASK_H__ */
