/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : sys_task.c
 * Author     : 上上签
 * Date       : 2023-12-02
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "sys_task.h"

#include <aos/kernel.h>
#include "ulog/ulog.h"

static aos_task_key_t task_key;
static int * p_task_loop_count[TASK_NUM];
const char* task_names[TASK_NUM] = 
{
    "task1",
    "task2",
};

void log_loop_count(void)
{
    int loop_count = *((int *)aos_task_getspecific(task_key));
    LOGI(MUDULE_NAME, "task name is %s,loopcount = %d\r\n", aos_task_name(), loop_count);
}

void task_entry(void *arg)
{
    //设置到任务的私有空间地址上
    aos_task_setspecific(task_key, arg);
    while(1)
    {
        (*(int *)arg)++; //任务计数值++
        log_loop_count();
        aos_msleep(1000);
    }
}


int sys_task_init(void)
{
    int index;
    int ret;
    aos_set_log_level(AOS_LL_INFO); // 设置ulog打印等级

    //创建私有数据key值
    ret = aos_task_key_create(&task_key);
    if(ret != 0)
    {
        LOGE(MUDULE_NAME, "key create is err\r\n");
    }

    for(index = 0; index < TASK_NUM; index++)
    {
        //为每个任务创建任务私有数据空间
        p_task_loop_count[index] =(int *)aos_zalloc(sizeof(int));
        if(p_task_loop_count[index] == NULL)
        {
            LOGE(MUDULE_NAME, "%s aos_zalloc is err\r\n", task_names[index]);
        }

        ret = aos_task_new(task_names[index], task_entry, p_task_loop_count[index], 4*256);
        if(ret != 0)
        {
            LOGE(MUDULE_NAME, "%s create is err\r\n", task_names[index]);
        }
    }
    return 0;
}
