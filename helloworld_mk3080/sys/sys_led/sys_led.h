/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : sys_led.h
 * Author     : 上上签
 * Date       : 2023-12-01
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __SYS_LED_H__
#define __SYS_LED_H__

#define RED_LED 0 // PA14
#define GRE_LED 1 // PA15

extern int sys_led_init(void);
extern int sys_led_output_toggle(int ledNum);
extern int sys_led_ctrl(int ledNum, int status);
#endif /* __SYS_LED_H__ */
