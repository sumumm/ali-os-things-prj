/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : sys_led.c
 * Author     : 上上签
 * Date       : 2023-12-01
 * Version    : 
 * Description: 
 * ======================================================
 */
#include <aos/kernel.h>
#include "aos/hal/gpio.h"

#include "sys_led.h"

static gpio_dev_t  led_gpio_dev[2] = {0};

int sys_led_init(void)
{
    //由于PA14时rtl8710的jtag接口，我要使用GPIO功能，必须先关闭jtag借口
    sys_jtag_off();
    led_gpio_dev[0].port = RED_LED;
    led_gpio_dev[0].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[0].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[0]);

    led_gpio_dev[1].port = GRE_LED;
    led_gpio_dev[1].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[1].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[1]);

    return 0;
}

int sys_led_output_toggle(int ledNum)
{
    hal_gpio_output_toggle(&led_gpio_dev[ledNum]);
    return 0;
}

int sys_led_ctrl(int ledNum, int status)
{

    if(status)
    {
        hal_gpio_output_high(&led_gpio_dev[ledNum]);
    }
    else
    {
        hal_gpio_output_low(&led_gpio_dev[ledNum]);
    }

    return 0;
}