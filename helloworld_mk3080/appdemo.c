/*
 * Copyright (C) 2015-2020 Alibaba Group Holding Limited
 */

#include <stdio.h>
#include <stdlib.h>
#include <aos/kernel.h>
#include "aos/init.h"
#include "board.h"
#include <k_api.h>

#include "sys_led.h"
#include "sys_key.h"
#include "sys_task.h"

int application_start(int argc, char *argv[])
{
    printf("++++++++++ nano entry here! ++++++++++\r\n");
    //fd = board_lcd_create("name");
    //board_lcd_write(fd,buffer,len);
    sys_led_init();    // LED初始化
    sys_key_int_init();// key初始化
    sys_task_init();   // 任务测试初始化
    while(1) {
        aos_msleep(500);
        sys_led_output_toggle(GRE_LED);
    };
}

