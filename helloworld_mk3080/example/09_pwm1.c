/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : 09_pwm1.c
 * Author     : 上上签
 * Date       : 2023-12-15
 * Version    : 
 * Description: 
 * ======================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <aos/kernel.h>
#include <k_api.h>
#include <string.h>

#include "aos/init.h"
#include "board.h"

#include "ulog/ulog.h"
#include "aos/hal/gpio.h"
#include "aos/hal/pwm.h"

#define PRT(...)                                          \
    do {                                                            \
        printf("\033[1;32;40m[LOG][%s][%d]: ", __func__, __LINE__);     \
        printf("\033[0m");                                      \
        printf(__VA_ARGS__);                                    \
    } while (0)
#define MK3080_PWM2 PORT_PWM_3
typedef enum __LED_PORT_{
    RED_LED = 0, // PA14
    GRE_LED = 1, // PA15
    BLU_LED = 6, // PA00
}LED_PORT;

static gpio_dev_t  led_gpio_dev[3] = {0};

int sys_led_init(void)
{
    //由于PA14时rtl8710的jtag接口，我要使用GPIO功能，必须先关闭jtag借口
    sys_jtag_off();
    led_gpio_dev[0].port = RED_LED;
    led_gpio_dev[0].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[0].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[0]);

    led_gpio_dev[1].port = GRE_LED;
    led_gpio_dev[1].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[1].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[1]);

    led_gpio_dev[2].port = BLU_LED;
    led_gpio_dev[2].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[2].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[2]);

    return 0;
}

int func_led_ctrl(int ledNum, int status)
{
    if(status)
    {
        hal_gpio_output_high(&led_gpio_dev[ledNum]);
    }
    else
    {
        hal_gpio_output_low(&led_gpio_dev[ledNum]);
    }

    return 0;
}

int func_led_output_toggle(int ledNum)
{
    hal_gpio_output_toggle(&led_gpio_dev[ledNum]);
    return 0;
}

/* output one pwm signal */
void hal_pwm_app_static_out(void)
{
    int cnt;
    int32_t ret;
    pwm_dev_t pwm = {0};

    printf(" hal_pwm_app_static_out start\r\n");

    pwm.port = MK3080_PWM2;
    pwm.config.freq = 1000;//1KHz --> T = 1/1000 = 0.001s = 1ms
    pwm.config.duty_cycle = 0.5;
    pwm.priv = NULL;

    ret = hal_pwm_init(&pwm);
    if(ret){
        printf("hal_pwm_init fail,ret:%d\r\n",ret);
        return;
    }

    hal_pwm_start(&pwm);

    aos_msleep(5000);

    hal_pwm_stop(&pwm);

    hal_pwm_finalize(&pwm);

    printf("hal_pwm_app_static_out end\r\n");
}

void hal_pwm_app_dynamic_out(void)
{
    int cnt;
    int32_t ret;
    pwm_dev_t pwm = {0};

    printf("hal_pwm_app_dynamic_out start\r\n");

    pwm.port = MK3080_PWM2;
    pwm.config.freq = 1000;
    pwm.config.duty_cycle = 0.00;
    pwm.priv = NULL;

    ret = hal_pwm_init(&pwm);
    if(ret){
        printf("hal_pwm_init fail,ret:%d\r\n",ret);
        return;
    }

    hal_pwm_start(&pwm);

    cnt = 10;
    while (cnt--) {
        printf("duty_cycle count up\r\n");
        pwm_config_t para = {0.000, 1000};
        for (int i = 0; i < 100; i++) {
            para.duty_cycle += 0.01;
            aos_msleep(10);
            hal_pwm_para_chg(&pwm, para);
        }

        printf("duty_cycle count down\r\n");

        para.duty_cycle = 1.0;
        para.freq = 1000;
        for (int i = 0; i < 100; i++) {
            para.duty_cycle -= 0.01;
            aos_msleep(10);
            hal_pwm_para_chg(&pwm, para);
        }
    }

    hal_pwm_stop(&pwm);

    hal_pwm_finalize(&pwm);

    printf("hal_pwm_app_dynamic_out end\r\n");
}

int application_start(int argc, char *argv[])
{
    sys_led_init();
    PRT("+++++++++ LED初始化完毕! ++++++++++\r\n");
    hal_pwm_app_static_out();
    hal_pwm_app_dynamic_out();
    while(1)
    {
        aos_msleep(1000);
        func_led_output_toggle(1);//绿灯闪烁提示系统正在运行
    }
  
}
