/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : 09_pwm2.c
 * Author     : 上上签
 * Date       : 2023-12-15
 * Version    : 
 * Description: 
 * ======================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <aos/kernel.h>
#include <k_api.h>
#include <string.h>

#include "aos/init.h"
#include "board.h"

#include "ulog/ulog.h"
#include "aos/hal/gpio.h"
#include "aos/hal/pwm.h"

#define MK3080_PWM2 PORT_PWM_3
#define PRT(...)                                          \
    do {                                                            \
        printf("\033[1;32;40m[LOG][%s][%d]: ", __func__, __LINE__);     \
        printf("\033[0m");                                      \
        printf(__VA_ARGS__);                                    \
    } while (0)

typedef enum __LED_PORT_{
    RED_LED = 0, // PA14
    GRE_LED = 1, // PA15
    BLU_LED = 6, // PA00
}LED_PORT;

static gpio_dev_t  led_gpio_dev[3] = {0};
static pwm_dev_t   pwm_dev         = {0};

int sys_led_init(void)
{
    //由于PA14时rtl8710的jtag接口，我要使用GPIO功能，必须先关闭jtag借口
    sys_jtag_off();
    led_gpio_dev[0].port = RED_LED;
    led_gpio_dev[0].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[0].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[0]);

    led_gpio_dev[1].port = GRE_LED;
    led_gpio_dev[1].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[1].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[1]);

    led_gpio_dev[2].port = BLU_LED;
    led_gpio_dev[2].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[2].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[2]);
    PRT("+++++++++ LED初始化完毕! ++++++++++\r\n");
    return 0;
}

int func_led_ctrl(int ledNum, int status)
{
    if(status)
    {
        hal_gpio_output_high(&led_gpio_dev[ledNum]);
    }
    else
    {
        hal_gpio_output_low(&led_gpio_dev[ledNum]);
    }

    return 0;
}

int func_led_output_toggle(int ledNum)
{
    hal_gpio_output_toggle(&led_gpio_dev[ledNum]);
    return 0;
}


void sys_pwm_init(void)
{
    int32_t ret;

    pwm_dev.port = MK3080_PWM2;
    pwm_dev.config.freq = 1000;
    pwm_dev.config.duty_cycle = 0.00;
    pwm_dev.priv = NULL;

    ret = hal_pwm_init(&pwm_dev);
    if(ret)
    {
        printf("hal_pwm_init fail,ret:%d\r\n",ret);
        return;
    }
    PRT("+++++++++ PWM初始化完毕! ++++++++++\r\n");
    return;
}

void func_pwm_start(void)
{
    hal_pwm_start(&pwm_dev);
    return;
}

void func_pwm_stop(void)
{
    hal_pwm_stop(&pwm_dev);
    return;
}

void func_pwm_duty_set(float dutyCycle)
{
    pwm_config_t para = {0};
    if(dutyCycle > 1)
    {
        printf("dutyCycle is in 0~1\r\n");
        return;
    } 
    para.duty_cycle = dutyCycle;
    para.freq = 1000;
    hal_pwm_para_chg(&pwm_dev, para);
    return;
}

void hal_pwm_app_dynamic_out(void)
{
    int cnt;
    
    sys_pwm_init();
    func_pwm_start();

    cnt = 10;
    while (cnt--) 
    {
        PRT("duty_cycle count up\r\n");
        pwm_config_t para = {0.000, 1000};
        for (int i = 0; i < 100; i++) 
        {
            para.duty_cycle = (float)i / 100;
            aos_msleep(20);
            func_pwm_duty_set(para.duty_cycle);
        }

        PRT("duty_cycle count down\r\n");

        para.duty_cycle = 1.0;
        para.freq = 1000;
        for (int i = 100; i > 0; i--) 
        {
            para.duty_cycle = (float)i / 100;
            aos_msleep(20);
            func_pwm_duty_set(para.duty_cycle);
        }
    }

    hal_pwm_stop(&pwm_dev);

    hal_pwm_finalize(&pwm_dev);

    PRT("hal_pwm_app_dynamic_out end\r\n");
}

int application_start(int argc, char *argv[])
{
    sys_led_init();
    
    hal_pwm_app_dynamic_out();
    while(1)
    {
        aos_msleep(1000);
        func_led_output_toggle(1);//绿灯闪烁提示系统正在运行
    }
  
}