/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : 07_work_queue_new.c
 * Author     : 上上签
 * Date       : 2023-12-09
 * Version    : 
 * Description: 
 * ======================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <aos/kernel.h>
#include <k_api.h>

#include "aos/init.h"
#include "board.h"

#include "ulog/ulog.h"
#include "aos/hal/gpio.h"
#include "aos/cli.h"


#define RED_LED     0 // PA14
#define GRE_LED     1 // PA15
#define MODULE_NAME "work_new_app" /* module name used by ulog */

static gpio_dev_t  led_gpio_dev[2] = {0}; 

static aos_work_t led_work;
static aos_workqueue_t message_work_queue;

static void work_entry(void *arg)
{
    LOGI(MODULE_NAME, "led[0] is toggle!");
    hal_gpio_output_toggle(&led_gpio_dev[0]);
    aos_work_run(&message_work_queue, &led_work);
}

int led_init(void)
{
    //由于PA14时rtl8710的jtag接口，我要使用GPIO功能，必须先关闭jtag借口
    sys_jtag_off();
    led_gpio_dev[0].port = RED_LED;
    led_gpio_dev[0].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[0].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[0]);

    led_gpio_dev[1].port = GRE_LED;
    led_gpio_dev[1].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[1].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[1]);

    return 0;
}

int application_start(int argc, char *argv[])
{
    int ret = 0;

    led_init();
    aos_set_log_level(AOS_LL_INFO);
    aos_cli_init();

    /**
     * Create a user-defined work queue.
     * workqueue: message_work_queue (aos_workqueue_t structure variable)
     * pri:       10(priority of the workqueue background task)
     * stack_size: 512(stack size of the workqueue background task)
     */
    ret = aos_workqueue_create(&message_work_queue, 10, 8*256);
    if (ret != 0) {
        LOGE(MODULE_NAME, "create workqueue error");
        return;
    }

    /* init the work. The work was executed with a delay of 1000ms after startup. */
    ret = aos_work_init(&led_work, work_entry, NULL, 2000);
    if (ret != 0) {
        LOGE(MODULE_NAME, "create work error!");
        return;
    }
    /**
     * start the work, using the newly created workqueue .
     */
    aos_work_run(&message_work_queue, &led_work);

    while(1)
    {
        aos_msleep(1000);
        hal_gpio_output_toggle(&led_gpio_dev[1]);
    }
    return 0;
}