/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : 04_queue_key_test.c
 * Author     : 上上签
 * Date       : 2023-12-06
 * Version    : 
 * Description: 
 * ======================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <aos/kernel.h>
#include "aos/init.h"
#include "board.h"
#include <k_api.h>
#include "aos/hal/gpio.h"
#include "ulog/ulog.h"

#define RED_LED   0 // PA14
#define GRE_LED   1 // PA15

#define KEY1_PORT 8 // PA5
#define KEY2_PORT 2 // PA22
#define KEY3_PORT 4 // PA23

#define TASK_KEY_NAME   "task_key"
#define TASK_QUEUE_NAME "task_queue"
#define MOUDLE_NAME     "key_led_queue"

#define MAX_QUEUE_LEN   10
static uint32_t key_queue_arr[MAX_QUEUE_LEN] = {0};
static aos_queue_t key_queue;

aos_task_t  task1_handle;
aos_task_t  task2_handle;

gpio_dev_t  led_gpio_dev[2] = {0}; 
gpio_dev_t  key_gpio_dev;
uint32_t    key_value;

void task_key_entry(void *arg)
{
    int ret = 0;
    const char * task_name;
    task_name = aos_task_name();
    while(1) 
    {
        hal_gpio_input_get(&key_gpio_dev, &key_value);
        if(!key_value)
        {
           //延时去抖动
           aos_msleep(10);
           //再次判断是否按下
           hal_gpio_input_get(&key_gpio_dev, &key_value);
           if(!key_value)
           {
                LOGI(MOUDLE_NAME, "[%s]key is down!send key down message", task_name);
                ret = aos_queue_send(&key_queue, &key_value, sizeof(key_value));
                if(ret != 0)
                {
                    LOGE(MOUDLE_NAME, "queue send error");
                }
                //等待按键松开
                do{
                    hal_gpio_input_get(&key_gpio_dev, &key_value);
                    aos_msleep(10);
                }while(!key_value);
                LOGI(MOUDLE_NAME, "[%s]key is up!send key up message", task_name);
                ret = aos_queue_send(&key_queue, &key_value, sizeof(key_value));
                if(ret != 0)
                {
                    LOGE(MOUDLE_NAME, "queue send error");
                }
           }
        }
        //10ms 读取一次按键状态
        aos_msleep(10);
    }

}

void task_queue_entry(void *arg)
{
    int ret = 0;
    uint32_t message[MAX_QUEUE_LEN] = {0};
    int size = 0;
    const char * task_name;
    task_name = aos_task_name();
    while(1)
    {
        ret = aos_queue_recv(&key_queue, AOS_WAIT_FOREVER, message, &size);
        if(ret == 0)
        {
            LOGI(MOUDLE_NAME, "[%s]recv message is %d", task_name, message[0]);
        }
    }

}
int led_init(void)
{
    //由于PA14时rtl8710的jtag接口，我要使用GPIO功能，必须先关闭jtag借口
    sys_jtag_off();
    led_gpio_dev[0].port = RED_LED;
    led_gpio_dev[0].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[0].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[0]);

    led_gpio_dev[1].port = GRE_LED;
    led_gpio_dev[1].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[1].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[1]);

    return 0;
}

int key_init(void)
{
    key_gpio_dev.port = KEY1_PORT;
    //根据原理图分析配置位输入上拉模式
    key_gpio_dev.config = INPUT_PULL_UP;
    key_gpio_dev.priv = NULL;
    //key 初始化
    hal_gpio_init(&key_gpio_dev);

    return 0;
}

int application_start(int argc, char *argv[])
{
    int ret = 0;
    int index = 0;

    led_init();
    key_init();
    aos_set_log_level(AOS_LL_INFO);

	//创建任务
    ret = aos_task_new(TASK_KEY_NAME, task_key_entry, NULL, 8*128);
    if(ret != 0)
    {
        LOGE(MOUDLE_NAME, "task key new error");
    }
    ret = aos_task_new(TASK_QUEUE_NAME, task_queue_entry, NULL, 8*128);
    if(ret != 0)
    {
        LOGE(MOUDLE_NAME, "task queue new error");
    }

    //创建消息队列
    ret = aos_queue_new(&key_queue, key_queue_arr, sizeof(key_queue_arr), sizeof(uint32_t));
    if(ret != 0)
    {
        LOGE(MOUDLE_NAME, "queue new error");
    }

    while(1)
    {
        aos_msleep(1000);
        hal_gpio_output_toggle(&led_gpio_dev[1]);
    }
  
}
