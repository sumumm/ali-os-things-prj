/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : 05_cli_led.c
 * Author     : 上上签
 * Date       : 2023-12-08
 * Version    : 
 * Description: 
 * ======================================================
 */
/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : 04_queue_key.c
 * Author     : 上上签
 * Date       : 2023-12-06
 * Version    : 
 * Description: 
 * ======================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <aos/kernel.h>
#include "aos/init.h"
#include "board.h"
#include <k_api.h>
#include "aos/hal/gpio.h"
#include "ulog/ulog.h"
#include "aos/cli.h"

#define RED_LED   0 // PA14
#define GRE_LED   1 // PA15

gpio_dev_t  led_gpio_dev[2] = {0}; 

static void led_help_show(void)
{
	printf("Usage: led [OPTIONS] COMMAND\r\n");
	printf("\r\n");
	printf("Options:\r\n");
	printf("help        :  show led help\r\n");
	printf("\r\n");
	printf("Command:\r\n");
	printf("on          :  open the led\r\n");
	printf("off         :  close the led\r\n");
}

static void led_cmd(char *buf, int32_t len, int32_t argc, char **argv)
{
	if(argc == 2)
	{
		if(strcmp(argv[0], "led"))
		{
			printf("cmd is error \r\n");
			printf("plese retry\r\n");
			return ;
		}

		if(strcmp(argv[1], "help") == 0)
		{
			led_help_show();
		}
		else if(strcmp(argv[1], "on") == 0)
		{
			//led on
			printf("led is on!\r\n");
        	hal_gpio_output_high(&led_gpio_dev[0]);
		}
		else if(strcmp(argv[1], "off") == 0)
		{
			//led off
			printf("led is off!\r\n");
       		hal_gpio_output_low(&led_gpio_dev[0]);
		}
		else
		{
			printf("led options error\r\n");
			printf("plese retry");
			return;
		}
	}
	else
	{
		printf("input argument error!plese retry!\r\n");
        led_help_show();
	}
}

void cmd_init(void)
{
	int ret;
	static struct cli_command cmds[] = {
		{"led","led help", led_cmd},
	};
	
	ret = aos_cli_register_commands(cmds, sizeof(cmds)/sizeof(cmds[0]));
	if(ret != 0)
	{
		printf("cli register error!\r\n");
	}
}

int led_init(void)
{
    //由于PA14时rtl8710的jtag接口，我要使用GPIO功能，必须先关闭jtag借口
    sys_jtag_off();
    led_gpio_dev[0].port = RED_LED;
    led_gpio_dev[0].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[0].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[0]);

    led_gpio_dev[1].port = GRE_LED;
    led_gpio_dev[1].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[1].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[1]);

    return 0;
}

int application_start(int argc, char *argv[])
{
    int ret = 0;
    int index = 0;

    led_init();

    aos_set_log_level(AOS_LL_INFO);

    aos_cli_init();
	cmd_init();

    while(1)
    {
        aos_msleep(1000);
        hal_gpio_output_toggle(&led_gpio_dev[1]);
    }
  
}
