/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : 03_mutex_intrpt_none.c
 * Author     : 上上签
 * Date       : 2023-12-06
 * Version    : 
 * Description: 
 * ======================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <aos/kernel.h>
#include "aos/init.h"
#include "board.h"
#include <k_api.h>
#include "aos/hal/gpio.h"
#include "ulog/ulog.h"

#define RED_LED   0 // PA14
#define GRE_LED   1 // PA15

#define KEY1_PORT 8 // PA5
#define KEY2_PORT 2 // PA22
#define KEY3_PORT 4 // PA23

#define TASK1_NAME 		 "task1"
#define TASK2_NAME 		 "task2"
#define APP_MOUDLE_NAME  "app_task"

aos_task_t  task1_handle;
aos_task_t  task2_handle;

gpio_dev_t  led_gpio_dev[2] = {0}; 
gpio_dev_t  key_gpio_dev;
uint32_t    key_value;

void task1_entry(void *arg)
{
    const char * task_name;
    task_name = aos_task_name();
	while(1)
	{
	 	printf("++++++++++%s test!++++++++++\r\n", task_name);
		aos_msleep(10);
	}
}

void task2_entry(void *arg)
{
    const char * task_name;
    task_name = aos_task_name();
	while(1)
	{
	 	printf("%s test 1\r\n", task_name);
	 	printf("%s test 2\r\n", task_name);
	 	printf("%s test 3\r\n", task_name);
	 	printf("%s test 4\r\n", task_name);
	 	printf("%s test 5\r\n", task_name);
	 	printf("%s test 6\r\n", task_name);
	 	printf("%s test 7\r\n", task_name);
	 	printf("%s test 8\r\n", task_name);
	 	printf("%s test 9\r\n", task_name);
	 	printf("%s test 10\r\n", task_name);
	 	printf("%s test 11\r\n", task_name);
	 	printf("%s test 12\r\n", task_name);
	 	printf("%s test 13\r\n", task_name);
	 	printf("%s test 14\r\n", task_name);
	 	printf("%s test 15\r\n", task_name);
	 	printf("%s test 16\r\n", task_name);
	 	printf("%s test 17\r\n", task_name);
	 	printf("%s test 18\r\n", task_name);
	 	printf("%s test 19\r\n", task_name);
	 	printf("%s test 20\r\n", task_name);
	}
}

int led_init(void)
{
    //由于PA14时rtl8710的jtag接口，我要使用GPIO功能，必须先关闭jtag借口
    sys_jtag_off();
    led_gpio_dev[0].port = RED_LED;
    led_gpio_dev[0].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[0].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[0]);

    led_gpio_dev[1].port = GRE_LED;
    led_gpio_dev[1].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[1].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[1]);

    return 0;
}

int application_start(int argc, char *argv[])
{
    int ret = 0;
    int index = 0;

    led_init();
    key_gpio_dev.port = KEY1_PORT;
    //根据原理图分析配置位输入上拉模式
    key_gpio_dev.config = INPUT_PULL_UP;
    key_gpio_dev.priv = NULL;
    //key 初始化
    hal_gpio_init(&key_gpio_dev);

    aos_set_log_level(AOS_LL_INFO);

    // 创建两个指定优先级的任务
    ret = aos_task_new_ext(&task1_handle, TASK1_NAME, task1_entry, NULL, 8*128, 58);
	if(ret != 0)
	{
		LOGE(APP_MOUDLE_NAME,"task key create err\r\n");
	}
	ret = aos_task_new_ext(&task2_handle, TASK2_NAME, task2_entry, NULL, 8*128, 59);
	if(ret != 0)
	{
		LOGE(APP_MOUDLE_NAME,"task led create err\r\n");
	}

    while(1)
    {
        aos_msleep(1000);
        hal_gpio_output_toggle(&led_gpio_dev[1]);
    }
  
}
