
#include <stdio.h>
#include <stdlib.h>
#include <aos/kernel.h>
#include <k_api.h>

#include "aos/init.h"
#include "board.h"

#include "ulog/ulog.h"
#include "aos/hal/gpio.h"
#include "aos/cli.h"



#define RED_LED     0 // PA14
#define GRE_LED     1 // PA15
#define MODULE_NAME "timer_app" /* module name used by ulog */

static aos_timer_t timer1; /* timer handle */
static gpio_dev_t  led_gpio_dev[2] = {0}; 
static int timer1_default = 1000;
int led_init(void)
{
    //由于PA14时rtl8710的jtag接口，我要使用GPIO功能，必须先关闭jtag借口
    sys_jtag_off();
    led_gpio_dev[0].port = RED_LED;
    led_gpio_dev[0].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[0].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[0]);

    led_gpio_dev[1].port = GRE_LED;
    led_gpio_dev[1].config = OUTPUT_PUSH_PULL;
    led_gpio_dev[1].priv = NULL;
    //gpio PA14初始化
    hal_gpio_init(&led_gpio_dev[1]);

    return 0;
}

/**
 * timer callback function.
 * The current use case is executed every 1300ms.
 */
static void timer1_func(void *timer, void *arg)
{
    /*
    * Warning: an interface that causes a block is not allowed to be called within this function.
    * Blocking within this function will cause other timers to run incorrectly.
    * The printf function may also block on some platforms, so be careful how you call it.
    */
    LOGI(MODULE_NAME, "[timer_new]timer expires!");
}

static void timer_help_show(void)
{
	printf("Usage: timer [OPTIONS] COMMAND\r\n");
	printf("\r\n");
	printf("Options:\r\n");
	printf("help        :  show timer help\r\n");
	printf("\r\n");
	printf("Command:\r\n");
	printf("start       :  start the timer\r\n");
	printf("stop        :  stop the timer\r\n");
	printf("change      :  change the timer's time\r\n");
}
static void timer_time_change(int nms)
{
    /* stop the timer before modifying the timer parameter */
    aos_timer_stop(&timer1);

    /* the timer cycle is modified to nms */
    aos_timer_change(&timer1, nms);

    /* start the timer after the timer parameter modification is complete */
    aos_timer_start(&timer1);
}
static void timer_cmd(char *buf, int32_t len, int32_t argc, char **argv)
{
    int nsecs = 0;
	if(argc == 2 || argc == 3)
	{
		if(strcmp(argv[0], "timer"))
		{
			printf("cmd is error \r\n");
			printf("plese retry\r\n");
			return ;
		}

		if(strcmp(argv[1], "help") == 0)
		{
			timer_help_show();
		}
		else if(strcmp(argv[1], "start") == 0)
		{
			//timer start
			printf("timer is start!\r\n");
        	aos_timer_start(&timer1);
		}
		else if(strcmp(argv[1], "stop") == 0)
		{
			//led off
			printf("timer is stop!\r\n");
       		aos_timer_stop(&timer1);
		}
        else if(strcmp(argv[1], "change") == 0)
		{
			//timer change
            if(argv[2] != NULL)
            {
                nsecs = atoi(argv[2]);
            }
            else
            {
                nsecs = timer1_default;
            }
            if(nsecs < 1)
            {
                nsecs = timer1_default;
            }
			printf("timer is change!nsecs=%d\r\n", nsecs);
       		timer_time_change(nsecs);
		}
		else
		{
			printf("timer options error\r\n");
			printf("plese retry");
			return;
		}
	}
	else
	{
		printf("input argument error!plese retry!\r\n");
        timer_help_show();
	}
}

void cmd_init(void)
{
	int ret;
	static struct cli_command cmds[] = {
		{"timer","timer help", timer_cmd},
	};
	
	ret = aos_cli_register_commands(cmds, sizeof(cmds)/sizeof(cmds[0]));
	if(ret != 0)
	{
		printf("cli register error!\r\n");
	}
}

int application_start(int argc, char *argv[])
{
    int ret = 0;
    int index = 0;

    led_init();

    aos_set_log_level(AOS_LL_INFO);

    aos_cli_init();
    cmd_init();
    /**
     * Create timer. Timer starts automatically after successful creation.
     * some of the parameters are as follows:
     * fn:      timer1_func (this function is called when the timer expires)
     * ms:      1500 (the cycle of the timer)
     * repeat:  1 (set to periodic timer)
     */
    ret = aos_timer_new(&timer1, timer1_func, NULL, timer1_default, 1);
    if (ret != 0) 
    {
        LOGE(MODULE_NAME, "create timer error!");
        return;
    }
    while(1)
    {
        aos_msleep(1000);
        hal_gpio_output_toggle(&led_gpio_dev[1]);
    }
  
}
