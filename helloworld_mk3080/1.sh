#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : 1.sh
# * Author     : 上上签
# * Date       : 2023-12-05
# * ======================================================
##

BIN_FILE=out/helloworld_mk3080@mk3080/binary/helloworld_mk3080@mk3080_ota.bin
TARGET_DIR=~/1sharedfiles

aos make
cp -pvf ${BIN_FILE} ${TARGET_DIR}